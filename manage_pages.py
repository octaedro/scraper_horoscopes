from pages import Page
from connection import Connection

class ManagePages:
	pages = []

	def scrap_and_save_page(self,site_name=""):
		connection = Connection()
		connection.get_details_type_from_bd(site_name)
		for one_sign in connection.signs:
			for one_site in connection.sites:
				one_page = Page()
				one_page.scrap_page(one_site,one_sign,connection.concepts)
				if one_page:
					self.pages.append(one_page)
		connection.save_predictions(self.get_predictions_from_pages(self.pages))
		return True

	def get_predictions_from_pages(self,pages):
		array_predictions = []
		for one_page in pages:
			for one_prediction in one_page.predictions:
				array_predictions.append(one_prediction)
		return array_predictions