# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup, Tag
import requests
from predictions import Prediction
from random import randint
from datetime import datetime

class Page:
	predictions = []

	def scrap_page(self,one_site,one_sign,concepts):
		URL = self.prepare_the_url(one_site,one_sign)

		# Realizamos la petición a la web
		req = requests.get(URL)

		# Comprobamos que la petición nos devuelve un Status Code = 200
		status_code = req.status_code
		if status_code == 200:
			# Pasamos el contenido HTML de la web a un objeto BeautifulSoup()
			html = BeautifulSoup(req.content, "html.parser")
			self.parse_page(html,one_site.get("sitio"),concepts)
			return True
		else:
			print "Status Code %d" % status_code
			return False

	def parse_page(self,html,page,concepts):
		if page == "lanacion":
			# Obtenemos todos los divs donde están las entradas
			entradas = html.find_all('article', {'class': 'hoy__descrip'})

			for i, entrada in enumerate(entradas):
				# Con el método "getText()" no nos devuelve el HTML
				todas_las_predicciones = entrada.find('dl', {'class': 'listaItems'})
				for item in todas_las_predicciones:
					if item.find('dt') != -1:
						if 'amor' in item.find('dt').getText().lower():
							item_amor = item.find('dd').getText()
						if 'riqueza' in item.find('dt').getText().lower():
							item_dinero = item.find('dd').getText()
						if 'bienestar' in item.find('dt').getText().lower():
							item_salud = item.find('dd').getText()

				tip = entrada.find('p').getText()

				merged_items = "%s\n%s\n%s\n%s" % (tip, item_amor, item_dinero, item_salud)

				id_concept = [2, 3, 5]

				array_predictions_tmp = [[item_amor, self.get_prediction_id_content(item_amor,concepts)],
										 [item_dinero, self.get_prediction_id_content(item_dinero,concepts)],
										 [item_salud, self.get_prediction_id_content(item_salud,concepts)],
										 [merged_items, id_concept[randint(0, 2)]]]

				for one_prediction_content in array_predictions_tmp:
					a_prediction = Prediction(one_prediction_content[0], one_prediction_content[1])
					self.predictions.append(a_prediction)
		elif page == "20minutos":
			# Obtenemos todos los divs donde están las entradas
			entradas = html.find_all('div', {'class': 'prediction'})

			for i, entrada in enumerate(entradas[0]):
				# Con el método "getText()" no nos devuelve el HTML
				if isinstance(entrada, Tag):
					prediction = entrada.getText()
					if len(prediction) > 80 and 'Ver la predicción de hoy' not in prediction.encode('utf8'):
						id_concept = self.get_prediction_id_content(prediction,concepts)
						a_prediction = Prediction(prediction, id_concept)
						self.predictions.append(a_prediction)

	def prepare_the_url(self,one_site,one_sign):
		site_name = one_site.get("sitio")
		if site_name == "lanacion":
			_the_url = one_site.get("url")
			URL = _the_url.replace("[signo]", one_sign)
		elif site_name == "20minutos":
			i = datetime.now()
			_the_url = one_site.get("url")
			_the_url = _the_url.replace("[signo]", one_sign)
			URL = _the_url.replace("[day/month/year]", i.strftime('%d/%m/%Y'))
		return URL

	def get_prediction_id_content(self,prediction,concepts):
		count = -1
		array_id_concepts = []
		for one_concept in concepts:
			count += 1
			if one_concept in prediction:
				array_id_concepts.append(count)
		arr_len = len(array_id_concepts)
		if arr_len > 0:
			return array_id_concepts[randint(0, arr_len-1)]
		else:
			return randint(0, 25)
