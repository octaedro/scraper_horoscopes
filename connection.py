import pymysql
import config as configuration
import requests
import json
import ast


class Connection:
    sites = {}
    signs = []
    concepts = []
    token = ""

    def __init__(self):
        self.db = pymysql.connect(
            host=configuration.dbhost,
            user=configuration.dbuser,
            passwd=configuration.dbpass,
            db=configuration.dbname,
            charset='utf8'
        )

    def get_details_type_from_bd(self,site_name=""):
        # you must create a Cursor object. It will let
        #  you execute all the queries you need
        cur = self.db.cursor()
        self.sites = self.get_sites_info(cur,site_name)
        self.signs = self.get_signs(cur)
        self.concepts = self.get_concepts(cur)
        self.db.close()
        return True

    def get_sites_info(self, cur, site_name=""):
        array_resp = []
        where_sentense = " WHERE activo = 1"
        if not site_name == "":
            where_sentense = " WHERE activo = 1 AND sitio='%s'" % (site_name)
        # Use all the SQL you like"
        sentense = 'SELECT * FROM py_tipos_scraping%s' % (where_sentense)
        cur.execute(sentense)
        # print all the first cell of all the rows
        for row in cur.fetchall():
            if not row[1] == "none":
                array_resp.append({"sitio":row[1],"url":row[2],"max_length":row[3]})
        return array_resp

    def get_signs(self, cur):
        array_resp = []
        # Use all the SQL you like
        cur.execute("SELECT * FROM py_signs_sign")
        # print all the first cell of all the rows
        for row in cur.fetchall():
            if not row[1] == "none":
                array_resp.append(row[1])
        return array_resp

    def get_concepts(self, cur):
        array_resp = []
        # Use all the SQL you like
        cur.execute("SELECT * FROM py_concepts_concept")
        # print all the first cell of all the rows
        for row in cur.fetchall():
            # if not row[1] == "none":
            array_resp.append(row[1])
        return array_resp

    def save_predictions(self,predictions):
        if self.isLoggedIn():
            for one_prediction in predictions:
                self.send_prediction_to_api(one_prediction)
        return True

    def isLoggedIn(self):
        if self.token == "":
            return self.makeLogin()
        else:
            return True

    def makeLogin(self):
        try:
            url = configuration.base_url + configuration.login_url
            headers = {"Content-Type": "application/json; charset=utf-8","Data-Type": "json"}
            data_login = json.dumps({"username": "octaedro", "password": "qwerty22"}, ensure_ascii=False)
            r = requests.post(url, data=data_login,auth=('user', 'pass'),headers=headers)
            data = ast.literal_eval(r.content)
            self.token = data["token"]
            if r.status_code == 200 or r.status_code == 201:
                print "Login -  success!"
            else:
                print "Error! -> " + r.reason

        except requests.exceptions.RequestException as e:
            raise e

        return True

    def send_prediction_to_api(self,prediction):
        try:
            url = configuration.base_url + configuration.create_predictions_url
            headers = {"Content-Type": "application/json; charset=utf-8",
                       "Data-Type": "json",
                       "crossDomain": "True",
                       "Authorization": "JWT " + self.token}
            data_login = json.dumps({"concept": prediction.id_concept, "content": prediction.content})
            r = requests.post(url, data=data_login, headers=headers)
            if r.status_code == 200 or r.status_code == 201:
                print "Post created -  success!"
            else:
                data = ast.literal_eval(r.content)
                try:
                    if not (data["detail"] is None):
                        print(r.status_code, data["detail"])
                except:
                    print(r.status_code, "No tiene detalle")


        except requests.exceptions.RequestException as e:
            raise e
